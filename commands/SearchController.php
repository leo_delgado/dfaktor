<?php

namespace app\commands;

use app\models\IsbnSearch;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Асинхронный запуск поиска ISBN
 */
class SearchController extends Controller
{
    public function actionRun() {

        $search = new IsbnSearch();
        $search->run();

        return ExitCode::OK;
    }
}
