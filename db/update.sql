CREATE TABLE `isbn_search_log` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `book_id` int(11) DEFAULT NULL COMMENT 'fk books.id',
    `old_values` varchar(2000) DEFAULT NULL,
    `new_values` varchar(2000) DEFAULT NULL,
    `created` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='результаты поиска номеров ISBN в books.desription_ru';

CREATE TABLE `isbn_search_queue` (
    `id` int(11) NOT NULL,
    `params` char(255) DEFAULT NULL,
    `status` enum('pending','running','success','error') DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;