(function ($) {
    var progress = 0,
        progressLabel = $('.js-search-progress-text'),
        progressBar = $('.js-search-progress-bar'),
        runButton = $('.js-search-run-button');

    function updateProgress() {
        $.ajax('/isbn-search/progress')
            .done(progressOk)
            .fail(progressFail)
    }

    function runSearch() {
        $('.js-progress').show();
        runButton.attr('disabled', true)
            .removeClass('btn-success')
            .addClass('btn-disabled');
        progressLabel.text('Initializing search ...');
        progressBar.removeClass('progress-bar-success')
        $.ajax('/isbn-search/run')
            .done(searchOk)
            .fail(progressFail)
    }

    function searchOk(response) {
        if(response.success) {
            updateProgress();
        }
        else {
            progressFail();
        }
    }


    function progressOk(response) {
        progress = response.progress;
        progressBar.attr('style', 'width:' + progress + '%;');
        if(progress < 100) {
            progressLabel.text("Search in progress: " + progress + ' %');
            setTimeout(updateProgress, 1000);
        }
        else {
            progressLabel.text('Search complete!');
            progressBar.addClass('progress-bar-success');
            runButton.attr('disabled', false)
                .addClass('btn-success')
                .removeClass('btn-disabled');
            $.pjax.reload({container: '#isbn-search-log-grid', data:{page:1}});
        }
    }

    function progressFail() {
        progressBar.addClass('progress-bar-danger');
        progressLabel.text('Search Failed');
    }

    runButton.on('click', runSearch)
})(jQuery);