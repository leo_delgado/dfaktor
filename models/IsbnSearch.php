<?php

namespace app\models;

use yii\db\Expression;

/**
 * Поиск номеров ISBN в описаниях книг
 * Требуется искать в описании книги "10 или 13 цифр разделенных любым количеством любых символов",
 * причем, могут быть и "левые разделители".
 * Предположительно, плохой разделитель ISBN может получиться в результате ошибок OCR с плохого скана,
 * поэтому разелителем считается группа до 3 нецифоровых символов, напр. тильда окруженная пробелами
 * запятые и переводы строк разделителями ISBN не считаются
 *
 * Алгоритм поиска:
 * 1. разделение всего списка книг на части для контроля
 *    времени выполнения отдельного поискового запроса,
 * 2. подготовка очереди заданий, обработка заданий
 * 3. поиск в БД подходящих описаний по упрощенному регулярному выражению
 * 4. выбор номеров ISBN из найденных описаний, валидация
 * 5. сравнение результатов поиска с имеющимися записями
 *    если найдены новые номера - обновление данных книги, запись в протоколе
 * @package app\models
 */
class IsbnSearch {
    const CHUNK_SIZE = 1000;

    /**
     * разделение всего списка книг на части ,
     * подготовка очереди заданий
     * @return $this
     */
    public function init() {
        $total = Books::find()->count();
        $numChunks = (int) ceil($total / self::CHUNK_SIZE);

        IsbnSearchQueue::deleteAll();
        for($i = 0; $i < $numChunks; $i++) {
            $task = new IsbnSearchQueue();
            $task->id = $i + 1;
            $task->params = json_encode([
                'offset' => $i * self::CHUNK_SIZE,
                'limit' => self::CHUNK_SIZE,
            ]);
            $task->status = IsbnSearchQueue::STATUS_PENDING;
            $task->save();
        }
        return $this;
    }

    /**
     * процент завершения - отношение числа завершенных заданий к общему числу заданий
     * @return int
     */
    public function getProgress() {
        $expr = new Expression("FLOOR(100 * sum(status in ('success', 'error')) / COALESCE (count(*), 1))");
        return (int) IsbnSearchQueue::find()->select($expr)->scalar();
    }

    /**
     * основоной цик поиска
     * выбор задания из очереди
     * обновление статуса задания
     */
    public function run() {
        while ($task = IsbnSearchQueue::getNext()) {
            $task->status = IsbnSearchQueue::STATUS_RUNNING;
            if($this->_handleTask($task)) {
                $task->status = IsbnSearchQueue::STATUS_SUCCESS;
            }
            else {
                $task->status = IsbnSearchQueue::STATUS_ERROR;
            }
            $task->save();
        }
    }

    /**
     * обработка задания
     * выбор описаний книг, содержащих номера, похожие на ISBN
     * обработка каждого описания
     * @param IsbnSearchQueue $task
     * @return bool
     */
    private function _handleTask(IsbnSearchQueue $task) {
        $params = json_decode($task->params);
        try {
            $books = $this->_findBooks($params->offset, $params->limit);

            /** @var Books $book */
            foreach($books as $book) {

                $isbnFound = $this->_findIsbn($book->description_ru);
                $this->_updateBook($book, $isbnFound);
            }
        }
        catch (\Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * Поиск подходящих описаний
     * @param $offset
     * @param $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    private function _findBooks($offset, $limit) {

        $rg = '([0-9][^\d\n]{0,3}){10,13}';

        $chunkQuery = Books::find()
            ->select("id")
            ->orderBy("id")
            ->limit($limit)
            ->offset($offset);

        return Books::find()->alias('b')
            ->innerJoin(["c" => $chunkQuery], 'c.id = b.id')
            ->where("b.description_ru REGEXP '$rg'")
            ->all();
    }

    /**
     * поиск номеров ISBN в описании
     * @param $source
     * @return array
     */
    private function _findIsbn($source) {

        $rg = '/(*UTF8)(?:[0-9][^\d\n,]{0,3}){13}|(?:[0-9][^\d\n,]{0,3}){10}/';

        $result = [];
        if(preg_match_all($rg, $source, $matches)) {
            foreach($matches[0] as $match) {
                $result[] = new Isbn($match);
            }
        }

        return $result;
    }

    /**
     * Обновление списка книг, логирование
     */
    private function _updateBook(Books $book, array $isbnItems) {

        $good = array_flip (
            array_filter (
                array_merge ([
                    $book->isbn,
                    $book->isbn2,
                    $book->isbn3,
                    ],
                    array_map (function ($v){ return trim($v); }, explode(',', $book->isbn4))
                )
            )
        );

        $wrong = array_flip (
            array_filter (
                array_map (function ($v){ return trim($v); }, explode(',', $book->isbn_wrong))
            )
        );

        /** @var Isbn $isbn */
        foreach($isbnItems as $isbn) {
            $value = $isbn->toString();
            if($isbn->isValid()) {
                $good[$value] = true;
            }
            else {
                $wrong[$value] = true;
            }
        }

        $good = array_keys($good);
        $wrong = array_keys($wrong);

        foreach(['isbn', 'isbn2', 'isbn3'] as $field) {
            if($value = (string) array_shift ($good)) {
                $book->$field = $value;
            }
        }
        $book->isbn4 = implode(', ', $good);
        $book->isbn_wrong = implode(', ', $wrong);

        $dirty = $book->getDirtyAttributes();
        if(count($dirty)) {
            $old = [];
            foreach($dirty as $name => $value) {
                $old[$name] = $book->getOldAttribute($name);
            }

            $entry = new IsbnSearchLog();
            $entry->old_values = json_encode($old, JSON_UNESCAPED_UNICODE);
            $entry->new_values = json_encode($dirty, JSON_UNESCAPED_UNICODE);
            $entry->book_id = $book->id;
            $entry->created = date('Y-m-d H:i:s');

            $success = $book->save();
            if($success) {
                $success = $entry->save();
            }
            if(!$success) {
                throw new \Exception("Ошибка сохранения резельтатов поиска");
            };
        }
    }

}