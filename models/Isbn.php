<?php

namespace app\models;

/**
 * Класс номера ISBN
 * Разбор текстового представления,
 * валидация 10 и 13-значных ISBN
 */
class Isbn
{
    private $string;
    private $digits = [];
    private $codeValid;
    private $delimiterValid;

    /**
     * Isbn constructor
     * Вся обработка данных в конструкторе
     * @param $string
     */
    public function __construct($string) {

        $this->delimiterValid = $this->_parseString($string);

        switch(count($this->digits)) {
            case 10: $v = $this->_validate10(); break;
            case 13: $v = $this->_validate13(); break;
            default: $v = false;
        }

        $this->codeValid = $v;
    }

    /**
     * Неправильный если "контрольная цифра неправильная или есть левые разделители",
     * @return bool
     */
    public function isValid() {
        return $this->codeValid && $this->delimiterValid;
    }

    /**
     * строчное представление
     * @return mixed
     */
    public function toString() {
        return $this->string;
    }

    /**
     * в строчное представление не включаются символы исходной строки
     * до первой и после последней цифры
     * @param $string
     * @return bool
     */
    private function _parseString($string) {
        $valid = true;
        $count = 0;
        $this->string = '';
        foreach(str_split($string) as $char) {
            if(is_numeric($char)) {
                $this->string .= $char;
                $this->digits[] = (int) $char;
                $count++;
            }
            else {
                if($count > 0 && $count < 13) {
                    $this->string .= $char;
                    if($char !== '-') {
                        $valid = false;
                    }
                }
            }
        }
        return $valid;
    }

    /**
     * валидация 10-значного ISBN
     * алгоритм с двойным аккумулятором, см.
     * https://en.wikipedia.org/wiki/International_Standard_Book_Number#ISBN-10_check_digit_calculation
     * @return bool
     */
    private function _validate10() {
        $sum = 0;
        $tmp = 0;
        for ($i = 0; $i < 10; $i++) {
            $tmp += $this->digits[$i];
            $sum += $tmp;
        }
        return $sum % 11 == 0;
    }

    /**
     * валидация 13-значного ISBN
     * https://en.wikipedia.org/wiki/International_Standard_Book_Number#ISBN-13_check_digit_calculation
     * @return bool
     */
    private function _validate13() {
        $sum = 0;
        for($i = 0; $i < 11; $i += 2) {
            $sum += $this->digits[$i] + 3 * $this->digits[$i + 1];
        }
        $code = 10 - $sum % 10;

        return $code == $this->digits[12];
    }
}