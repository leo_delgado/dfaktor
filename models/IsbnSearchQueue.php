<?php

namespace app\models;

use Yii;

/**
 * Очередь заданий поиска ISBN
 *
 * @property int $id
 * @property string $params
 * @property string $status
 */
class IsbnSearchQueue extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'isbn_search_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['status'], 'string'],
            [['params'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'params' => 'Params',
            'status' => 'Status',
        ];
    }

    public static function getNext() {
        return self::findOne(['status' => self::STATUS_PENDING]);
    }

}
