<?php

namespace app\models;

/**
 * Список книг
 *
 * @property int $id
 * @property string $title_ru
 * @property string $description_ru
 * @property string $isbn
 * @property string $isbn2
 * @property string $isbn3
 * @property string $isbn4
 * @property string $isbn_wrong
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['description_ru'], 'string'],
            [['title_ru', 'isbn', 'isbn2', 'isbn3', 'isbn4', 'isbn_wrong'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'description_ru' => 'Description Ru',
            'isbn' => 'Isbn',
            'isbn2' => 'Isbn2',
            'isbn3' => 'Isbn3',
            'isbn4' => 'Isbn4',
            'isbn_wrong' => 'Isbn Wrong',
        ];
    }
}
