<?php

namespace app\models;

use Yii;

/**
 * Протокол результатов поиска ISBN
 *
 * @property int $id
 * @property int $book_id fk books.id
 * @property string $old_values
 * @property string $new_values
 * @property string $created
 */
class IsbnSearchLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'isbn_search_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id'], 'integer'],
            [['created'], 'safe'],
            [['old_values', 'new_values'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'old_values' => 'Old Values',
            'new_values' => 'New Values',
            'created' => 'Created',
        ];
    }
}
