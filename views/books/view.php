<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = 'view book';
\yii\web\YiiAsset::register($this);
?>
<div class="books-view">

    <h1><?= Html::encode($model->title_ru) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_ru',
            'description_ru:ntext',
            'isbn',
            'isbn2',
            'isbn3',
            'isbn4',
            'isbn_wrong',
        ],
    ]) ?>

</div>
