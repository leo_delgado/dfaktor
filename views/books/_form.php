<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'isbn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn_wrong')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
