<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\IsbnSearchLog;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ISBN Search';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="isbn-search-log-index">

    <h1>
        <?= Html::encode('ISBN Search Results') ?>
        <?= Html::button('Run new search', ['class' => 'js-search-run-button btn btn-success']) ?>

    </h1>

    <div class="js-progress" style="display: none;">
        <h4 class="js-search-progress-text">
            Search in progress
        </h4>
        <div class="progress">
            <div class="progress progress-striped active">
                <div class="js-search-progress-bar progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span id="js-search-progress-text" class="sr-only"></span>
                </div>
            </div>
        </div>
    </div>

    <?php Pjax::begin(['id' => 'isbn-search-log-grid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'book_id',
            ['attribute' => 'old_values', 'content' => function(IsbnSearchLog $item) {
                $res = '';
                foreach(json_decode($item->old_values) as $name => $value){
                    $res .= "<p><b>$name</b>: $value</p>";
                }
                return $res;
            }],
            ['attribute' => 'new_values', 'content' => function(IsbnSearchLog $item) {
                $res = '';
                foreach(json_decode($item->new_values) as $name => $value){
                    $res .= "<p><b>$name</b>: $value</p>";
                }
                return $res;
            }],
            'created',

            [   'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => ['view' => function ($url, $model, $key) {
                    return "<a target='_blank' href='/books/view?id=$model->book_id' data-pjax='0'><span class='glyphicon glyphicon-eye-open'></span></a>";
                 }],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<a target="_blank"