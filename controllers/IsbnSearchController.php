<?php

namespace app\controllers;

use app\models\IsbnSearch;
use app\models\IsbnSearchLog;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Управление поиском ISBN и просмотр результатов
 */
class IsbnSearchController extends Controller
{

    /**
     * Страница результатов поиска
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => IsbnSearchLog::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Запуск поиска в отдельном процессе через консольный контроллер
     * @return \yii\web\Response
     */
    public function actionRun() {
        $search = new IsbnSearch();
        $search->init();

        $command = \Yii::getAlias('@app/yii');
        exec("php $command search/run >/dev/null 2>/dev/null &");

        return $this->asJson(['success' => true]);
    }

    /**
     * Процент завершения поиска
     * @return \yii\web\Response
     */
    public function actionProgress() {
        $value = (new IsbnSearch())->getProgress();
        return $this->asJson(['progress' => $value]);
    }
}
